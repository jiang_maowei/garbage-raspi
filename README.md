
这是来自绵阳师范学院信息工程学院17级5班汤琳老师物联网工程课程的课堂作业.
##### 成员:
##### 组长:陈云
##### 组员:蒋茂苇
>比赛需要故只开源了粗劣的第一个版本demo实现,第二版本改进使用yoloV3模型进行垃圾分类检测,机器臂分拣垃圾,垃圾分类数据集重新收集,并有微信小程序的用户查询垃圾分类及反馈机制
注意看ReadMe文件,注意看ReadMe文件,注意看ReadMe文件
#### B站视频介绍地址：https://www.bilibili.com/video/av80830870
### 材料清单
> 1. 树莓派 1个
>
> 2. pca9685 16路舵机驱动板 1个
>
> 3. 7寸可触摸显示屏一个
>
> 4. MG996R 舵机4个
> 5. 垃圾桶4个
> 6. usb免驱动摄像头1个
> 7. 树莓派GPIO扩展板转接线柱1个
> 8. 硅胶航模导线若干

### 环境需求
#####     训练好的模型文件best.h5地址：https://pan.baidu.com/s/16JyXn9StHv_65RCcg2agcw 提取码: ifhy
### 1.开发环境

> 神经网络搭建—python 依赖 tensorflow,keras
>
> 训练图片来源华为云2019垃圾分类大赛提供
>
> > 训练图片地址：https://developer.huaweicloud.com/hero/forum.php?mod=viewthread&tid=24106
> >
> > 下载图片文件后将文件解压覆盖为 garbage_classify 放入 垃圾分类-本地训练/根目录
>
> 神经网络开源模型--- resnet50 
>
> > models 目录需要手动下载resnet50 的模型文件放入
> >
> > resnet50模型文件名：resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5
> >
> > 百度就可以找到下载放入即可：https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5
>
> 

### 2.运行开发环境

>  进入 "垃圾分类-本地训练"目录

> # 环境初始化
>
> - python3
> - 安装框架flask`pip3 install flask`
> - 安装tensorflow，keras等依赖 
>
> > - `pip3 install tensorflow==1.13.1`
> > - `pip3 install keras==2.3.1 `
>
> # 运行
>
> - 1.命令`python3 train.py`开启训练
> - 2.命令`python3 predict_local.py`开启输入图片测试
>
> ###### 

### 3. 训练服务模型部署 

> 进入 "垃圾分类-服务部署"目录
>
> 1. output_model 目录存放的是本地训练完成导出的h5模型文件
>
> 2. models 目录需要手动下载resnet50 的模型文件放入
>
>    > resnet50模型文件名：resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5
>    >
>    > 百度就可以找到下载放入即可：https://github.com/fchollet/deep-learning-models/releases/download/v0.2/resnet50_weights_tf_dim_ordering_tf_kernels_notop.h5

> # 环境初始化
>
> - 安装框架flask`pip3 install flask`
> - 安装tensorflow，keras等依赖 
>
> > - `pip3 install tensorflow==1.13.1`
> > - `pip3 install keras==2.3.1 `
>
> # 运行
>
> - 1.命令`python3 run.py`开启窗口本地调试
> - 2.命令`python3 flask_sever.py`开启服务部署
> - 3.命令`sh ./start.sh`开启后台运行服务部署

### 4.树莓派界面搭建

> 基于nodejs electron-vue 
>
> 强烈建议使用cnpm来安装nodejs库
>
> 进入 "树莓派端/garbage_desktop"目录
>
> # 安装依赖
> cnpm install
>
> # 开发模式
> cnpm run dev
>
> # 打包发布
> cnpm run build

### 5.树莓派端flask-api接口操作硬件

> 进入"进入 "树莓派端/garbage_app_sever"目录"
>
> 注意树莓派应该开启I2C，确保pca9685 I2C方式接入后可显示地址
>
> > 命令：i2cdetect -y 1 
> >
> > 查看 地址项 0x40是否已经接入树莓派
>
> 运行 python3 app_sever.py 或者 sh start.sh 启动
>
> 若提示缺少依赖：
>
> 1. pip3 install adafruit-*pca9685*
> 2. pip3 install flask

